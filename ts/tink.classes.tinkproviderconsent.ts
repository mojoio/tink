import * as plugins from './tink.plugins.js';

export interface IProviderData {
  credentialsId: string;
  providerName: string;
  status: string;
  sessionExpiryDate: number;
  accountIds: string[];
  statusUpdated: number;
}

import { TinkUser } from './tink.classes.tinkuser.js';

/**
 * a provider consent maps to tinks bank consents
 */
export class TinkProviderConsent {
  public static async getProviderUserAccessToken(tinkUserRefArg: TinkUser) {
    const authorizationCode = await tinkUserRefArg.tinkAccountRef.getUserAuthorizationCode(
      tinkUserRefArg.externalUserIdArg,
      tinkUserRefArg.tinkAccountRef.clientId,
      'accounts:read,balances:read,transactions:read,provider-consents:read'
    );
    const accessToken = await tinkUserRefArg.tinkAccountRef.getUserAccessToken(authorizationCode);
    return accessToken;
  }

  // STATIC
  public static async getProviderConsentsForUser(tinkUserRefArg: TinkUser) {
    const returnProviderConsents: TinkProviderConsent[] = [];
    const accessToken = await this.getProviderUserAccessToken(tinkUserRefArg);
    const responseData = await tinkUserRefArg.tinkAccountRef.request({
      urlArg: '/api/v1/provider-consents',
      accessToken,
      methodArg: 'GET',
      payloadArg: null,
    });
    // console.log(responseData); // no nextPageToken here?
    if (responseData.providerConsents) {
      for (const providerConsentData of responseData.providerConsents) {
        returnProviderConsents.push(new TinkProviderConsent(tinkUserRefArg, providerConsentData));
      }
    }
    return returnProviderConsents;
  }

  // INSTANCE
  tinkUserRef: TinkUser;
  data: IProviderData;
  constructor(tinkUserRefArg: TinkUser, dataArg: IProviderData) {
    this.tinkUserRef;
    this.data = dataArg;
  }

  /**
   * refresh the bank account data from origin
   */
  public async refresh() { 
    const userAccessToken = await TinkProviderConsent.getProviderUserAccessToken(this.tinkUserRef);
    const response = await this.tinkUserRef.tinkAccountRef.request({
      accessToken: userAccessToken,
      methodArg: 'POST',
      urlArg: `https://api.tink.com/api/v1/credentials/${this.data.credentialsId}/refresh`,
      payloadArg: null
    });
    
    // polling as per documentation for status 'UPDATED';
    let pollingRounds = 0;
    let status = null;
    while(status !== 'UPDATED' && pollingRounds < 10) {
      await plugins.smartdelay.delayFor(2000);
      await this.update();
      status = this.data.status;
      pollingRounds++;
    }
    // think about how to handle errors here
  }

  public async update() {
    const providerConsents = await TinkProviderConsent.getProviderConsentsForUser(this.tinkUserRef);
    for (const providerConsent of providerConsents) {
      if (providerConsent.data.credentialsId === this.data.credentialsId) {
        this.data = providerConsent.data;
      }
    }
  }
}
