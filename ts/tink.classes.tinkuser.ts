import * as plugins from './tink.plugins.js';

import { TinkAccount } from './tink.classes.tinkaccount.js';
import { TinkProviderConsent } from './tink.classes.tinkproviderconsent.js';
import { BankAccount } from './tink.classes.bankaccount.js';

export class TinkUser {
  // STATIC
  public static async createNewTinkUser(tinkAccountArg: TinkAccount, externalUserIdArg: string) {
    const accessToken = await tinkAccountArg.getClientAccessTokenForScope('user:create');
    const responseData: {
      external_user_id: string;
      user_id: string;
    } = await tinkAccountArg.request({
      urlArg: '/api/v1/user/create',
      accessToken,
      methodArg: 'POST',
      payloadArg: {
        external_user_id: externalUserIdArg,
        market: 'DE',
        locale: 'en_US',
      },
    });

    const newTinkUser = await TinkUser.getTinkUser(tinkAccountArg, externalUserIdArg);
    return newTinkUser;
  }

  public static async getTinkUser(tinkAccountArg: TinkAccount, externalUserIdArg: string) {
    const authorizationCode = await tinkAccountArg.getUserAuthorizationCode(
      externalUserIdArg,
      tinkAccountArg.clientId,
      'user:read'
    );
    const accessToken = await tinkAccountArg.getUserAccessToken(authorizationCode);
    const responseData: {
      appId: string;
      created: string;
      externalUserId: string;
      flags: string[];
      id: string;
      nationalId: string;
      profile: {
        // cashbackEnabled: boolean; // deprecated
        currency: string;
        locale: string;
        market: string;
        notificationSettings: {
          balance: boolean;
          budget: boolean;
          doubleCharge: boolean;
          einvoices: boolean;
          fraud: boolean;
          income: boolean;
          largeExpense: boolean;
          leftToSpend: boolean;
          loanUpdate: boolean;
          summaryMonthly: boolean;
          summaryWeekly: boolean;
          transaction: boolean;
          unusualAccount: boolean;
          unusualCategory: boolean;
        };
        periodAdjustedDay: 25;
        periodMode: 'MONTHLY_ADJUSTED' | 'MONTHLY';
        timeZone: string;
      };
      // username: string; // not relevant
    } = await tinkAccountArg.request({
      urlArg: '/api/v1/user',
      accessToken,
      methodArg: 'GET',
      payloadArg: null,
    });
    const newTinkUser = new TinkUser(tinkAccountArg, responseData.id, responseData.externalUserId);
    return newTinkUser;
  }

  // INSTANCE
  public tinkAccountRef: TinkAccount;
  public tinkUserId: string;
  public externalUserIdArg: string;

  constructor(tinkAccountrefArg: TinkAccount, tinkUserIdArg: string, externalUserIdArg: string) {
    this.tinkAccountRef = tinkAccountrefArg;
    this.tinkUserId = tinkUserIdArg;
    this.externalUserIdArg = externalUserIdArg;
  }

  /**
   * deletes the user
   */
  public async delete() {
    const authorizationCode = await this.tinkAccountRef.getUserAuthorizationCode(
      this.externalUserIdArg,
      this.tinkAccountRef.clientId,
      'user:delete'
    );
    const accessToken = await this.tinkAccountRef.getUserAccessToken(authorizationCode);
    const response = await this.tinkAccountRef.request({
      methodArg: 'POST',
      accessToken,
      payloadArg: {},
      urlArg: '/api/v1/user/delete',
    });
    console.log(`successfully deleted user with externalId ${this.externalUserIdArg}`);
  }

  /**
   * gets a tink link that can be used by a user to connect accounts
   * @returns
   */
  public async getTinkLinkForMarket(linkOptionsArg: {
    countryId: string;
    redirectUrl: string;
    /**
     * an optional state that is transported through to the callback
     */
    customState: string;
    testProviderBool?: boolean;
  } = {
    countryId: 'NL',
    redirectUrl: 'https://console.tink.com/callback',
    customState: "exampleState",
    testProviderBool: true
  }): Promise<string> {
    if (typeof linkOptionsArg.testProviderBool !== 'boolean') {
      linkOptionsArg.testProviderBool = false;
    }
    const authorizationCode = await this.tinkAccountRef.getUserAuthorizationCode(
      this.externalUserIdArg,
      'df05e4b379934cd09963197cc855bfe9', // this is a hardcoded app id for tink link, as recommended by tink.com
      'authorization:read,authorization:grant,credentials:refresh,credentials:read,credentials:write,providers:read,user:read'
    );
    const tinkUrlOptions: {[key: string]: string} = {}
    tinkUrlOptions['client_id'] = this.tinkAccountRef.clientId;
    tinkUrlOptions['redirect_uri']= linkOptionsArg.redirectUrl;
    tinkUrlOptions['authorization_code'] = authorizationCode;
    tinkUrlOptions['market'] = linkOptionsArg.countryId;
    if (linkOptionsArg.testProviderBool) {
      tinkUrlOptions['test'] = 'true';
    }
    if (linkOptionsArg.customState) {
      tinkUrlOptions['state'] = linkOptionsArg.customState;
    }
    const url = plugins.smarturl.Smarturl.createFromUrl('https://link.tink.com/1.0/business-transactions/connect-accounts', {
      searchParams: tinkUrlOptions
    });
    const tinkLinkUrl = url.toString();
    return tinkLinkUrl;
  }

  public async getProviderConsents(): Promise<TinkProviderConsent[]> {
    const providerConsents: TinkProviderConsent[] =
      await TinkProviderConsent.getProviderConsentsForUser(this);
    return providerConsents;
  }

  /**
   * gets all accounts
   */
  public async getAllBankAccounts() {
    const bankAccounts = await BankAccount.getBankAccountsForUser(this);
    return bankAccounts;
  }
}
