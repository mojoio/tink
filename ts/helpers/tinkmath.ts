export interface ITinkScaledAmount {
  value: {
    unscaledValue: string;
    scale: string;
  };
  currencyCode: string;
}

/**
 * returns a normalized amount
 * @param scaledArg
 * @returns
 */
export const getNormalizedAmount = (scaledArg?: ITinkScaledAmount) => {
  if (!scaledArg) {
    return null;
  }
  return {
    value: parseInt(scaledArg.value.unscaledValue) * Math.pow(10, -(parseInt(scaledArg.value.scale))),
    currency: 'EUR'
  };
};
