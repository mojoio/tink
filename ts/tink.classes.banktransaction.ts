import { BankAccount } from './tink.classes.bankaccount.js';
import * as plugins from './tink.plugins.js';

import * as tinkHelpers from './helpers/index.js';

export interface ITinkBankTransactiondata {
  id: string;
  accountId:string;
  amount: {
    currencyCode: string;
    value: {
      scale: string;
      unscaledValue: string;
    };
  };
  categories: {
    pfm: {
      id: string;
      name: string;
    };
  };
  dates: {
    booked: string;
    value: string;
  };
  descriptions: {
    display: string;
    original: string;
  };
  identifiers: {
    providerTransactionId: string;
  };
  merchantInformation: {
    merchantCategoryCode: 'string';
    merchantName: 'string';
  };
  providerMutability: string;
  reference: string;
  status: string;
  types: {
    financialInstitutionTypeCode: string;
    type: string;
  };
}

export class BankTransaction {
  // STATIC
  public static async getTransactionAccessToken(bankAccountArg: BankAccount) {
    const authorizationCode =
      await bankAccountArg.tinkUserRef.tinkAccountRef.getUserAuthorizationCode(
        bankAccountArg.tinkUserRef.externalUserIdArg,
        bankAccountArg.tinkUserRef.tinkAccountRef.clientId,
        'accounts:read,balances:read,transactions:read,provider-consents:read'
      );
    const accessToken = await bankAccountArg.tinkUserRef.tinkAccountRef.getUserAccessToken(
      authorizationCode
    );
    return accessToken;
  }

  public static async getBankTransactions(bankAccountArg: BankAccount) {
    const accessToken = await this.getTransactionAccessToken(bankAccountArg);
    const pageSize = 100;
    const returnTransactions: BankTransaction[] = [];
    const getTransactionsRecursively = async (nextPageToken?: string) => {
      const searchParams = new URLSearchParams();
      searchParams.set('accountIdIn', bankAccountArg.data.id);
      searchParams.set('pageSize', '200');
      if (nextPageToken) {
        searchParams.set('pageToken', nextPageToken)
      }
      const response = await bankAccountArg.tinkUserRef.tinkAccountRef.request({
        urlArg: `/data/v2/transactions?${searchParams.toString()}`,
        accessToken: accessToken,
        methodArg: 'GET',
        payloadArg: null,
      });
      for (const transaction of response.transactions) {
        returnTransactions.push(new BankTransaction(bankAccountArg, transaction));
      }

      if (response.nextPageToken.length > 0) {
        await getTransactionsRecursively(response.nextPageToken);
      }
    };
    await getTransactionsRecursively();
    return returnTransactions;
  }

  // INSTANCE
  bankAccountRef: BankAccount;
  data: ITinkBankTransactiondata;

  constructor(bankAccountRefArg: BankAccount, dataArg: ITinkBankTransactiondata) {
    this.bankAccountRef = bankAccountRefArg;
    this.data = dataArg;
  }

  /**
   * gets normalized data
   */
  public getNormalizedData() {
    return {
      id: this.data.id,
      date: new Date(this.data.dates.booked).getTime(),
      amount: tinkHelpers.getNormalizedAmount(this.data.amount),
      name: this.data.descriptions.display,
      description: this.data.descriptions.original,
      originAccountId: this.data.accountId,
      justForLooks: {
        originalScaledAmount: this.data.amount,
        dateIso: new Date(this.data.dates.booked).toISOString()
      }
    }

  }
}
