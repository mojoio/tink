import * as plugins from './tink.plugins.js';

import { TinkUser } from './tink.classes.tinkuser.js';

export class TinkAccount {
  public clientId: string;
  private _clientSecret: string;

  private _apiBaseUrl: string = 'https://api.tink.com';

  constructor(clientIdArg: string, clientSecretArg: string) {
    this.clientId = clientIdArg;
    this._clientSecret = clientSecretArg;
  }

  public async getTinkHealthyBoolean(): Promise<boolean> {
    const response = await plugins.smartrequest.request(
      'https://api.tink.com/api/v1/monitoring/healthy',
      {
        keepAlive: false,
      }
    );
    return response.body === 'ok';
  }

  public async getClientAccessTokenForScope(scopeArg: string): Promise<string> {
    // lets get an accessToken for the request
    const response = await plugins.smartrequest.postFormDataUrlEncoded(
      `${this._apiBaseUrl}/api/v1/oauth/token`,
      {
        keepAlive: false,
      },
      [
        {
          key: 'client_id',
          content: this.clientId,
        },
        {
          key: 'client_secret',
          content: this._clientSecret,
        },
        {
          key: 'grant_type',
          content: 'client_credentials',
        },
        {
          key: 'scope',
          content: scopeArg,
        },
      ]
    );
    if (response.statusCode !== 200) {
      console.log(response.statusCode);
      console.log(response.body);
      throw new Error('there was an error aquiring an access token.');
    }
    const clientAccessToken = response.body.access_token;
    return clientAccessToken;
  }

  public async getUserAuthorizationCode(
    externalUserIdArg: string,
    actorCLientIdArg: string,
    scopeArg: string
  ) {
    const accessToken = await this.getClientAccessTokenForScope('authorization:grant');
    const response = await plugins.smartrequest.postFormDataUrlEncoded(
      `${this._apiBaseUrl}/api/v1/oauth/authorization-grant/delegate`,
      {
        keepAlive: false,
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      },
      [
        {
          key: 'response_type',
          content: 'code',
        },
        {
          key: 'actor_client_id',
          content: actorCLientIdArg,
        },
        {
          key: 'external_user_id',
          content: externalUserIdArg,
        },
        {
          key: 'id_hint',
          content: 'Hello there',
        },
        {
          key: 'scope',
          content: scopeArg,
        },
      ]
    );

    if (response.statusCode !== 200) {
      console.log(response.body);
      throw new Error('there was an error aquiring an access token.');
    }
    const userAuthorizationCode = response.body.code;
    return userAuthorizationCode;
  }

  public async getUserAccessToken(authorizationCode: string): Promise<string> {
    const accessToken = await this.getClientAccessTokenForScope('authorization:grant');
    const response = await plugins.smartrequest.postFormDataUrlEncoded(
      `${this._apiBaseUrl}/api/v1/oauth/token`,
      {
        keepAlive: false,
      },
      [
        {
          key: 'code',
          content: authorizationCode,
        },
        {
          key: 'client_id',
          content: this.clientId,
        },
        {
          key: 'client_secret',
          content: this._clientSecret,
        },
        {
          key: 'grant_type',
          content: 'authorization_code',
        },
      ]
    );

    if (response.statusCode !== 200) {
      console.log(response.body);
      throw new Error('there was an error aquiring an access token.');
    }
    const userAccessToken = response.body.access_token;
    return userAccessToken;
  }

  // the request method for tink respecting platform specific stuff
  // e.g. certain headers if needed
  public async request(optionsArg: {
    urlArg: string;
    methodArg: 'POST' | 'GET';
    accessToken: string;
    payloadArg: any;
  }) {
    // check health
    if (!(await this.getTinkHealthyBoolean())) {
      throw new Error('TINK is not healthy right now. Please try again later.');
    } else {
      console.log('tink is healthy, continuing...');
    }
    const response = await plugins.smartrequest.request(`${this._apiBaseUrl}${optionsArg.urlArg}`, {
      keepAlive: false,
      headers: {
        Authorization: `Bearer ${optionsArg.accessToken}`,
        'Content-Type': 'application/json',
      },
      method: optionsArg.methodArg,
      requestBody: JSON.stringify(optionsArg.payloadArg),
    });
    console.log(response.statusCode);
    return response.body;
  }

  public async getTinkUser(externalUserIdArg: string) {
    const tinkuser = await TinkUser.getTinkUser(this, externalUserIdArg);
    return tinkuser;
  }

  public async createTinkUser(externalUserIdArg: string) {
    const tinkuser = await TinkUser.createNewTinkUser(this, externalUserIdArg);
    return tinkuser;
  }
}
