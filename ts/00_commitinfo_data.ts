/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@apiclient.xyz/tink',
  version: '3.1.11',
  description: 'an unofficial api abstraction for tink.com'
}
