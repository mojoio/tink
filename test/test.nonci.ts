import { expect, expectAsync, tap } from '@push.rocks/tapbundle';
import * as qenv from '@push.rocks/qenv';

const testQenv = new qenv.Qenv('./', './.nogit/');

import * as tink from '../ts/index.js';

let tinkTestAccount: tink.TinkAccount;

tap.test('should create a valid tink account', async () => {
  tinkTestAccount = new tink.TinkAccount(
    await testQenv.getEnvVarOnDemand('TINK_CLIENT_ID'),
    await testQenv.getEnvVarOnDemand('TINK_CLIENT_SECRET')
  );
  expect(tinkTestAccount).toBeInstanceOf(tink.TinkAccount);
});

tap.test('should report tink as healthy', async () => {
  await expectAsync(tinkTestAccount.getTinkHealthyBoolean()).toBeTrue();
});

tap.test('should create a tink user', async (toolsArg) => {
  await tinkTestAccount.createTinkUser('user_1234_abc');
})

tap.test('should create a valid request', async (toolsArg) => {
  const tinkuser: tink.TinkUser = await tinkTestAccount.getTinkUser('user_1234_abc');
  console.log(tinkuser);
  console.log(await tinkuser.getTinkLinkForMarket()); // defaults to 'DE';
  console.log(await tinkuser.getProviderConsents());
});

tap.test('allow tink link to be used', async (toolsArg) => {
  await toolsArg.delayFor(30000);
});

tap.test('get provider consents', async () => {
  const tinkuser: tink.TinkUser = await tinkTestAccount.getTinkUser('user_1234_abc');
  const providerConsents = await tinkuser.getProviderConsents();
  console.log(providerConsents);
});

tap.test('get bankaccounts', async (toolsArg) => {
  const tinkuser: tink.TinkUser = await tinkTestAccount.getTinkUser('user_1234_abc');
  const bankAccounts = await tinkuser.getAllBankAccounts();
  console.log(bankAccounts.map(bankAccountArg => bankAccountArg.getNormalizedData()));

  for (const bankAccount of bankAccounts) {
    const transactions = await bankAccount.getTransactions();
    for (const transaction of transactions) {
      console.log(`=======================`)
      console.log(JSON.stringify(transaction.getNormalizedData()));
    }
    await toolsArg.delayFor(10000);
  } 
});

tap.test('should delete existing users', async () => {
  expect(tinkTestAccount).toBeInstanceOf(tink.TinkAccount);
  const tinkUser = new tink.TinkUser(tinkTestAccount, null, 'user_1234_abc');
  await tinkUser.delete();
});

tap.start();
